<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/12/9 15:49
 */

require_once __DIR__ . '/vendor/autoload.php';

use Calculation\Payment\paypal\Pay;

$config = [
    // ID
    'clientId' => 'AX1C3aZ3TOTEYVlG8GAoo99ia6g-PCwiA_vZfWjmkUIAUzAuwpk28BCKF0fHnxNlzdIsJLfU9fsWNZFO',
    // 秘钥
    'clientSecret' => 'EGeyZj_WmKiekr47-pbDtqbE0rq1XqiOY4X0gPd48KXjILAc8m0rgnzzkuakX5y7msdhZwBIq9QF1ZgA',
    // 沙箱ID
    // 'sandboxClientId' => 'AQ04K346i9kc5OGustUrN4dOKsOuMezeAGohFSngjId9j4IxC-x2LZF6dBGQ7XFb62B7AluCOuN_73IA',
    // 沙箱秘钥
    // 'sandboxClientSecret' => 'EAPmqSsYo7HP4cu6DyWYBhxBb5JEINUORlZZudwcp2uXo7QFEioU2MAkcm11an8JwPPcqX7krQSyjoPY',
    // 模式(live | sandbox) 开发or沙箱
    'mode' => 'live',
    // http代理
    'httpProxy' => '', //47.243.118.133:8989
    // http代理连接超时
    'httpConnectionTimeOut' => 30,
    // 异步通知地址
    'notifyUrl' => 'http://newcaixi.com/api/v1/pal_notify',
    // 同步通知地址
    'returnUrl' => 'http://newcaixi.com/api/v1/pal_return',
];

$order = [
    'body' => '测试购买商品',
    'goods_name' => '人教版小学数学二年级',
    'order_id' => rand(100000000000, 9999999999999),
    'goods_total_price' => 1,
    'currency' => 'HKD', //货币类型 新台币（TWD） 港币（HKD）
];

$payPalPay = new Pay();
$payPalPay->setOptions($config);
// PayPal支付
// $res = $payPalPay->pay($order);

// 查询订单
$paymentId = 'PAYID-MOKUJQY4XF54556TA8259713';
$PayerID = '2UE7D9RQPWB5G';
$res = $payPalPay->query($paymentId, $PayerID);
var_dump($res);die;