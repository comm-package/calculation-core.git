<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/12/7 15:06
 */

namespace Calculation\Questionnaire;

use Calculation\Questionnaire\Question as QuestionClass;

/**
 * Class 计算问卷调查的区间
 */
class IArea
{
    public static $dataPool = [
        'red' => [],
        'blue' => [],
        'yellow' => [],
        'green' => [],
        'firm' => [],
        'expression' => [],
        'flexible' => [],
    ];

    /**
     * 终结计算
     * @param array $askList 用户选择的答案
     * @param array $questionCountList 问卷题目，规则
     * @param string $logStr
     * @param int $type
     * @return array
     */
    public static function finalityComputeCondition($askList = [], $questionCountList = [], $type = 0, &$logStr = '')
    {
        $userResult = []; //结蛤集
        $redCount = 0; //红色的值
        $blueCount = 0; //蓝色的值
        $greenCount = 0; //绿色的值
        $yellowCount = 0; //黄色的值
        //1、计算红蓝黄绿
        foreach ($questionCountList as $key => $value) {
            $logStr1 = '';
            $isColourArr = ['red' => 1, 'blue' => 1, 'yellow' => 1, 'green' => 1];
            if (empty($askList[$key]['answer'])) {
                throw new \ErrorException('答案不能为空');
            }
            //勾选的颜色
            if ($value['red']) {
                $redValue = QuestionClass::setBlueRedYellowCount($askList[$key]['answer']);
                $redCount += $redValue;
                $logStr1 .= '-- 红色：' . $redValue . PHP_EOL;
                //throw new ApiSuccessException($redCount);
                unset($isColourArr['red']);
            }
            if ($value['blue']) {
                $blueValue = QuestionClass::setBlueRedYellowCount($askList[$key]['answer']);
                $blueCount += $blueValue;
                $logStr1 .= '-- 蓝色：' . $blueValue . PHP_EOL;

                unset($isColourArr['blue']);
            }
            if ($value['yellow']) {
                $yellowValue = QuestionClass::setBlueRedYellowCount($askList[$key]['answer']);
                $yellowCount += $yellowValue;
                $logStr1 .= '-- 黄色：' . $yellowValue . PHP_EOL;
                unset($isColourArr['yellow']);
            }
            if ($value['green']) {
                $greenValue = QuestionClass::setGreenCount($askList[$key]['answer']);
                $greenCount += $greenValue;
                $logStr1 .= '-- 绿色：' . $greenValue . PHP_EOL;
                unset($isColourArr['green']);
            }
            if (count($isColourArr) < 4) {
                //其他颜色
                $fractionalVal = $askList[$key]['answer'];
                $addNumber = QuestionClass::setOtherColorCount($fractionalVal);
                $logStr1 .= '-- 其他色：' . $addNumber . PHP_EOL;
                foreach ($isColourArr as $key_1 => $value_1) {
                    $colourName = $key_1 . 'Count';
                    $$colourName += $addNumber;
                }

                $str = '';
                $str .= $value['blue'] ? '蓝色' : '';
                $str .= $value['yellow'] ? '黄色' : '';
                $str .= $value['red'] ? '红色' : '';
                $str .= $value['green'] ? '绿色' : '';
                $keys = $key + 1;

                $logStr .= '题目号：' . $keys . ' 所勾选的颜色:' . $str . '----- 分数值:' . $fractionalVal . ' 红色:' . $redCount . ' 蓝色:' . $blueCount . ' 黄色' . $yellowCount . ' 绿色' . $greenCount . PHP_EOL;
                $logStr .= $logStr1;
            }
        }

        $userResult['data']['red'] = $redCount; //红色
        $userResult['data']['green'] = $greenCount; //绿色
        $userResult['data']['blue'] = $blueCount; //蓝色
        $userResult['data']['yellow'] = $yellowCount; //黄色

        //特质公用
        $antonymArr = [1 => 7, 2 => 6, 3 => 5, 4 => 4, 5 => 3, 6 => 2, 7 => 1]; //反题答案定义


        //2、计算坚定、表达、变通
        $firmInfluenceArr = [30, 63, 58, 84, 87]; //坚定影响题目
        $firmCount = 0;  //坚定的分数
        $firmArr = []; //坚定影响题目分数
        $lowFirm = 0; //坚定低分的次数
        $heightFirm = 0; //坚定高分的次数
        $firmLog = '';  //坚定日志

        $expressionInfluenceArr = [25, 28, 31, 73, 80]; //表达影响的题目
        $expressionRelevant = [93]; //特殊相关影响题目
        $expressionCount = 0; //表达的分数
        $expressionArr = []; //表达影响题目分数
        $expressionLog = '';  //表达日志

        $flexibleInfluenceArr = [11, 51, 89, 23]; //变通影响题目
        $flexibleArr = []; //变通影响题目分数
        $flexibleOtherArr = []; //其他问题条数
        $flexibleCount = 0;  //变通的分数
        $flexibleLog = ''; //变通日志

        //获取相关影响分数
        foreach ($questionCountList as $key => $value) {
            $answerId = $key + 1; //第几题标识
            $answerVal = $askList[$key]['answer'];
            if (array_key_exists($answerId, $expressionRelevant)) {
                $expressionArr[$answerId] = $answerVal;
            }
            $keys = $key + 1;
            //返题结果
            $firmAnswer = $value['antonym'] ? $antonymArr[$answerVal] : $answerVal;
            $answerText = $value['antonym'] ? '(反:' . $answerVal . ')' : '';
            //坚定
            if ($value['firm']) {
                if (in_array($answerId, $firmInfluenceArr)) {
                    $actualVal = QuestionClass::setConfirmCount($firmAnswer);
                    $firmArr[] = $firmAnswer;//问卷答案得分
                    $firmCount += $actualVal;
                    if ($firmAnswer != 5) {
                        if ($firmAnswer > 5) {
                            $heightFirm = $heightFirm + 1;
                        } else {
                            $lowFirm = $lowFirm + 1;
                        }
                    }

                } else {
                    $firmCount += QuestionClass::setConfirmCount($firmAnswer);
                }

                $firmLog .= '题目号：' . $keys . ' 勾选了坚定-----答案(' . $answerVal . ') 分数值' . $answerText . ':' . $firmAnswer . ' 坚定分数:' . $firmCount . ' 低分答案个数：' . $lowFirm . ' 高分答案个数：' . $heightFirm . PHP_EOL;
            }
            //表达
            if ($value['expression']) {
                if (in_array($answerId, $expressionInfluenceArr)) {
                    $expressionArr[$answerId] = $firmAnswer;
                }
                $expressionCount += $firmAnswer;

                $expressionLog .= '题目号：' . $keys . ' 勾选了表达----- 分数值' . $answerText . ':' . $firmAnswer . ' 表达分数:' . $expressionCount . PHP_EOL;
            }
            //变通
            if ($value['flexible']) {
                if (in_array($answerId, $flexibleInfluenceArr)) {
                    $flexibleArr[$answerId] = $answerVal;
                } else {
                    $flexibleOtherArr[] = $answerVal;
                }
                $flexibleCount += $answerVal;

                $flexibleLog .= '题目号：' . $keys . ' 勾选了变通—---- 分数值:' . $answerVal . ' 表通分数:' . $flexibleCount . PHP_EOL;
            }
        }

        //坚定最终计分
        $firmData = QuestionClass::setConfirmLowHeight($firmCount, $lowFirm, $heightFirm, $firmArr);
        $firmCount = $firmData['count'];
        $firmLog .= $firmData['log'] . PHP_EOL;
        $firmLog .= '坚定的最终得分是：' . $firmCount . PHP_EOL;

        //表达最终计分
        $expression = QuestionClass::setExpresstionCount($expressionCount, $expressionArr);
        $expressionCount = $expression['value'];
        $expressionLog .= '表达的最终得分是：' . $expression['str'] . PHP_EOL;

        //变通最终计分
        $flexibleResult = QuestionClass::setFlexibleCount($flexibleCount, $flexibleArr, $flexibleOtherArr);
        $flexibleCount = $flexibleResult['value'];
        $flexibleLog .= '变通的最终得分是：' . $flexibleResult['str'] . PHP_EOL;

        //第三个地方的 条形状分数值
        $userResult['data']['firm'] = $firmCount;
        $userResult['data']['expression'] = $expressionCount;
        $userResult['data']['flexible'] = $flexibleCount;

        $userResult['log']['img'] = $logStr;
        $userResult['log']['firm'] = $firmLog;
        $userResult['log']['expression'] = $expressionLog;
        $userResult['log']['flexible'] = $flexibleLog;

        return $userResult;
    }

    /**
     * 终结计算 青年版（修改）
     * @param array $askList 用户选择的答案
     * @param array $questionCountList 问卷题目，规则
     * @param string $logStr
     * @param int $type
     * @return array
     */
    public static function finalityComputeConditionByYouth($askList = [], $questionCountList = [], $type = 0, &$logStr = '')
    {
        $userResult = []; //结蛤集
        $redCount = 0; //红色的值
        $blueCount = 0; //蓝色的值
        $greenCount = 0; //绿色的值
        $yellowCount = 0; //黄色的值
        //1、计算红蓝黄绿
        foreach ($questionCountList as $key => $value) {
            $logStr1 = '';
            $isColourArr = ['red' => 1, 'blue' => 1, 'yellow' => 1, 'green' => 1];
            if (empty($askList[$key]['answer'])) {
                throw new \ErrorException('答案不能为空');
            }
            //勾选的颜色
            if ($value['red']) {
                $redValue = QuestionClass::setBlueRedYellowCount($askList[$key]['answer']);
                $redCount += $redValue;
                $logStr1 .= '-- 红色：' . $redValue . PHP_EOL;
                //throw new ApiSuccessException($redCount);
                unset($isColourArr['red']);
            }
            if ($value['blue']) {
                $blueValue = QuestionClass::setBlueRedYellowCount($askList[$key]['answer']);
                $blueCount += $blueValue;
                $logStr1 .= '-- 蓝色：' . $blueValue . PHP_EOL;

                unset($isColourArr['blue']);
            }
            if ($value['yellow']) {
                $yellowValue = QuestionClass::setBlueRedYellowCount($askList[$key]['answer']);
                $yellowCount += $yellowValue;
                $logStr1 .= '-- 黄色：' . $yellowValue . PHP_EOL;
                unset($isColourArr['yellow']);
            }
            if ($value['green']) {
                $greenValue = QuestionClass::setGreenCount($askList[$key]['answer']);
                $greenCount += $greenValue;
                $logStr1 .= '-- 绿色：' . $greenValue . PHP_EOL;
                unset($isColourArr['green']);
            }
            if (count($isColourArr) < 4) {
                //其他颜色
                $fractionalVal = $askList[$key]['answer'];
                $addNumber = QuestionClass::setOtherColorCount($fractionalVal);
                $logStr1 .= '-- 其他色：' . $addNumber . PHP_EOL;
                foreach ($isColourArr as $key_1 => $value_1) {
                    $colourName = $key_1 . 'Count';
                    $$colourName += $addNumber;
                }

                $str = '';
                $str .= $value['blue'] ? '蓝色' : '';
                $str .= $value['yellow'] ? '黄色' : '';
                $str .= $value['red'] ? '红色' : '';
                $str .= $value['green'] ? '绿色' : '';
                // $keys = $key + 1;
                $keys = $value['number'];

                $logStr .= '题目号：' . $keys . ' 所勾选的颜色:' . $str . '----- 分数值:' . $fractionalVal . ' 红色:' . $redCount . ' 蓝色:' . $blueCount . ' 黄色' . $yellowCount . ' 绿色' . $greenCount . PHP_EOL;
                $logStr .= $logStr1;
            }
        }

        $userResult['data']['red'] = $redCount; //红色
        $userResult['data']['green'] = $greenCount; //绿色
        $userResult['data']['blue'] = $blueCount; //蓝色
        $userResult['data']['yellow'] = $yellowCount; //黄色

        //特质公用
        $antonymArr = [1 => 7, 2 => 6, 3 => 5, 4 => 4, 5 => 3, 6 => 2, 7 => 1]; //反题答案定义


        //2、计算坚定、表达、变通
        $firmInfluenceArr = [30, 63, 58, 84, 87]; //坚定影响题目
        $firmCount = 0;  //坚定的分数
        $firmArr = []; //坚定影响题目分数
        $lowFirm = 0; //坚定低分的次数
        $heightFirm = 0; //坚定高分的次数
        $firmLog = '';  //坚定日志

        $expressionInfluenceArr = [25, 28, 31, 73, 80]; //表达影响的题目
        $expressionRelevant = [93]; //特殊相关影响题目
        $expressionCount = 0; //表达的分数
        $expressionArr = []; //表达影响题目分数
        $expressionLog = '';  //表达日志

        $flexibleInfluenceArr = [11, 51, 89, 23]; //变通影响题目
        $flexibleArr = []; //变通影响题目分数
        $flexibleOtherArr = []; //其他问题条数
        $flexibleCount = 0;  //变通的分数
        $flexibleLog = ''; //变通日志

        //获取相关影响分数
        foreach ($questionCountList as $key => $value) {
            $answerId = $key + 1; //第几题标识
            $answerId = $value['number'];#这个才是第几题标识 （青年版有的题目为空所有标识重新找）
            $answerVal = $askList[$key]['answer'];#答案
            if (array_key_exists($answerId, $expressionRelevant)) {
                $expressionArr[$answerId] = $answerVal;
            }
            #$keys = $key+1;
            $keys = $value['number'];
            //返题结果
            $firmAnswer = $value['antonym'] ? $antonymArr[$answerVal] : $answerVal;
            $answerText = $value['antonym'] ? '(反:' . $answerVal . ')' : '';
            //坚定
            if ($value['firm']) {
                if (in_array($answerId, $firmInfluenceArr)) {
                    $actualVal = QuestionClass::setConfirmCount($firmAnswer);

                    $firmArr[] = $firmAnswer;//问卷答案得分
                    $firmCount += $actualVal;
                    if ($firmAnswer != 5) {
                        if ($firmAnswer > 5) {
                            $heightFirm = $heightFirm + 1;
                        } else {
                            $lowFirm = $lowFirm + 1;
                        }
                    }

                } else {
                    $firmCount += QuestionClass::setConfirmCount($firmAnswer);
                }

                $firmLog .= '题目号：' . $keys . ' 勾选了坚定-----答案(' . $answerVal . ') 分数值' . $answerText . ':' . $firmAnswer . ' 坚定分数:' . $firmCount . ' 低分答案个数：' . $lowFirm . ' 高分答案个数：' . $heightFirm . PHP_EOL;
            }
            //表达
            if ($value['expression']) {
                if (in_array($answerId, $expressionInfluenceArr)) {
                    $expressionArr[$answerId] = $firmAnswer;
                }
                $expressionCount += $firmAnswer;

                $expressionLog .= '题目号：' . $keys . ' 勾选了表达----- 分数值' . $answerText . ':' . $firmAnswer . ' 表达分数:' . $expressionCount . PHP_EOL;
            }
            //变通
            if ($value['flexible']) {
                if (in_array($answerId, $flexibleInfluenceArr)) {
                    $flexibleArr[$answerId] = $answerVal;
                } else {
                    $flexibleOtherArr[] = $answerVal;
                }
                $flexibleCount += $answerVal;

                $flexibleLog .= '题目号：' . $keys . ' 勾选了变通—---- 分数值:' . $answerVal . ' 表通分数:' . $flexibleCount . PHP_EOL;
            }
        }

        //坚定最终计分
        $firmData = QuestionClass::setConfirmLowHeight($firmCount, $lowFirm, $heightFirm, $firmArr);
        $firmCount = $firmData['count'];
        $firmLog .= $firmData['log'] . PHP_EOL;
        $firmLog .= '坚定的最终得分是：' . $firmCount . PHP_EOL;

        //表达最终计分
        $expression = QuestionClass::setExpresstionCount($expressionCount, $expressionArr);
        $expressionCount = $expression['value'];
        $expressionLog .= '表达的最终得分是：' . $expression['str'] . PHP_EOL;

        //变通最终计分
        $flexibleResult = QuestionClass::setFlexibleCount($flexibleCount, $flexibleArr, $flexibleOtherArr);
        $flexibleCount = $flexibleResult['value'];
        $flexibleLog .= '变通的最终得分是：' . $flexibleResult['str'] . PHP_EOL;

        //第三个地方的 条形状分数值
        $userResult['data']['firm'] = $firmCount;
        $userResult['data']['expression'] = $expressionCount;
        $userResult['data']['flexible'] = $flexibleCount;

        $userResult['log']['img'] = $logStr;
        $userResult['log']['firm'] = $firmLog;
        $userResult['log']['expression'] = $expressionLog;
        $userResult['log']['flexible'] = $flexibleLog;

        return $userResult;
    }

    /**
     * 计算区间
     * @param string $color 颜色 red blue yellow green
     * @param array $userSelect 用户选择的结果
     * @return float
     */
    public static function getRedBlueYellowGreenArea($color = '', $userSelect = [])
    {
        $resultList = self::getCacheDataPool($color);
        //1、先把数组升序
        sort($resultList);
        //2、找到用户选择的的分数值的key值
        $key = array_search($userSelect[$color], $resultList);
        //3、计算区间
        $area = count($resultList) === 0 ? 0 : round($key / count($resultList) * 100);
        if ($area < 5) $area = 5;
        if ($area > 95) $area = 95;
        return $area;
    }

    /**
     * 计算区间
     * @param string $type 特质
     * @param array $userSelect 用户选择的结果
     * @return float
     */
    public static function getFirmAndExpressionArea($type = '', $userSelect = [])
    {
        $resultList = self::getCacheDataPool($type);
        //1、先把数组升序
        sort($resultList);
        //2、找到用户选择的的分数值的key值
        $key = array_search($userSelect[$type], $resultList);
        //3、计算区间
        $area = count($resultList) === 0 ? 0 : round($key / count($resultList) * 100);
        if ($area < 5) $area = 5;
        if ($area > 95) $area = 95;
        return $area;
    }

    /**
     * 设置缓存数据池
     * @param $data
     */
    public static function setCacheDataPool($data)
    {
        self::$dataPool = $data;
    }

    /**
     * 获取缓存数据池
     * @param $type
     * @return array
     */
    public static function getCacheDataPool($type)
    {
        $data = self::$dataPool;
        $data = $data[$type];
        return $data;
    }
}