<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/12/8 18:11
 */

namespace Calculation\Payment\wxpay;


class Pay
{
    private $config;

    public function __construct()
    {
        $this->config = new Config();
    }

    /**
     * 统一下单 wap(h5)支付
     * @param array $params
     * @return string|null
     */
    public function unifiedOrderForWap($params)
    {
        // 支付数据
        $data['out_trade_no'] = $params['order_id'];
        $data['total_fee'] = (int)($params['goods_total_price'] * 100);
        $data['body'] = mb_substr($params['body'], 0, 42);
        $data['spbill_create_ip'] = $_SERVER["REMOTE_ADDR"];
        $data['attach'] = '微信支付';
        $data['appid'] = $this->config->appId;
        $data['mch_id'] = $this->config->mchId;
        $data['nonce_str'] = rand(000000000000, 999999999999);
        $data['trade_type'] = "MWEB";
        // $data['scene_info'] = '{"h5_info": {"type":"Wap","wap_url": "http://in-monkeys.com","wap_name": "盛猴子"}}';

        // 设置同步与异步
        $return_url = isset($params['return_url']) ? $params['return_url'] . '?out_trade_no=' . $data['out_trade_no'] : '';
        $data['notify_url'] = $this->config->notifyUrl;

        $sign = $this->toUrlParams($data);
        $dataXML = "<xml>
           <appid>" . $data['appid'] . "</appid>
           <attach>" . $data['attach'] . "</attach>
           <body>" . $data['body'] . "</body>
           <mch_id>" . $data['mch_id'] . "</mch_id>
           <nonce_str>" . $data['nonce_str'] . "</nonce_str>
           <notify_url>" . $data['notify_url'] . "</notify_url>
           <out_trade_no>" . $data['out_trade_no'] . "</out_trade_no>
           <spbill_create_ip>" . $data['spbill_create_ip'] . "</spbill_create_ip>
           <total_fee>" . $data['total_fee'] . "</total_fee>
           <trade_type>" . $data['trade_type'] . "</trade_type>
           <sign>" . $sign . "</sign>
        </xml>";

        $url = 'https://api.mch.weixin.qq.com/pay/unifiedorder';
        $result = $this->httpsPost($url, $dataXML);
        $ret = $this->xmlToArray($result);

        return $ret;
    }

    /**
     * 统一下单 JSAPI(小程序)支付
     * User: lmg
     * Date: 2022/12/9 15:26
     * @param $params
     * @return mixed
     */
    public function unifiedOrderForApplets($params)
    {
        $data['appid'] = $this->config->appId;
        $data['mch_id'] = $this->config->mchId;
        $data['attach'] = '微信支付';
        $data['nonce_str'] = rand(000000000000, 999999999999);
        $data['body'] = mb_substr($params['body'], 0, 42);
        $data['openid'] = $params['openid'];
        $data['out_trade_no'] = $params['order_id'];
        $data['total_fee'] = (int)($params['goods_total_price'] * 100);
        $data['spbill_create_ip'] = $_SERVER["REMOTE_ADDR"];
        $data['trade_type'] = "JSAPI";

        // 设置同步与异步
        $data['notify_url'] = $this->config->notifyUrl;

        $sign = $this->toUrlParams($data);
        $dataXML = "<xml>
           <appid>" . $data['appid'] . "</appid>
           <attach>" . $data['attach'] . "</attach>
           <body>" . $data['body'] . "</body>
           <mch_id>" . $data['mch_id'] . "</mch_id>
           <nonce_str>" . $data['nonce_str'] . "</nonce_str>
           <notify_url>" . $data['notify_url'] . "</notify_url>
           <openid>" . $data['openid'] . "</openid>
           <out_trade_no>" . $data['out_trade_no'] . "</out_trade_no>
           <spbill_create_ip>" . $data['spbill_create_ip'] . "</spbill_create_ip>
           <total_fee>" . $data['total_fee'] . "</total_fee>
           <trade_type>" . $data['trade_type'] . "</trade_type>
           <sign>" . $sign . "</sign>
        </xml>";
        $url = 'https://api.mch.weixin.qq.com/pay/unifiedorder';
        $result = $this->httpsPost($url, $dataXML);
        $ret = $this->xmlToArray($result);

        return $ret;
    }

    /**
     * 统一下单 JSAPI(公众号)支付
     * @param $params
     * @return mixed
     */
    public function unifiedOrderForPublic($params)
    {
        $data['appid'] = $this->config->publicAppId;
        $data['mch_id'] = $this->config->mchId;
        $data['attach'] = '微信支付';
        $data['nonce_str'] = rand(000000000000, 999999999999);
        $data['body'] = mb_substr($params['body'], 0, 42);
        $data['openid'] = $params['openid'];
        $data['out_trade_no'] = $params['order_id'];
        $data['total_fee'] = (int)($params['goods_total_price'] * 100);
        $data['spbill_create_ip'] = $_SERVER["REMOTE_ADDR"];
        $data['trade_type'] = "JSAPI";

        // 设置同步与异步
        $data['notify_url'] = $this->config->notifyUrl;

        $sign = $this->toUrlParams($data);
        $dataXML = "<xml>
           <appid>" . $data['appid'] . "</appid>
           <attach>" . $data['attach'] . "</attach>
           <body>" . $data['body'] . "</body>
           <mch_id>" . $data['mch_id'] . "</mch_id>
           <nonce_str>" . $data['nonce_str'] . "</nonce_str>
           <notify_url>" . $data['notify_url'] . "</notify_url>
           <openid>" . $data['openid'] . "</openid>
           <out_trade_no>" . $data['out_trade_no'] . "</out_trade_no>
           <spbill_create_ip>" . $data['spbill_create_ip'] . "</spbill_create_ip>
           <total_fee>" . $data['total_fee'] . "</total_fee>
           <trade_type>" . $data['trade_type'] . "</trade_type>
           <sign>" . $sign . "</sign>
        </xml>";
        $url = 'https://api.mch.weixin.qq.com/pay/unifiedorder';
        $result = $this->httpsPost($url, $dataXML);
        $ret = $this->xmlToArray($result);

        return $ret;
    }

    /**
     * 统一下单 NATIVE(扫码)支付
     * @param $params
     * @return mixed
     */
    public function unifiedOrderForPublicNATIVE($params)
    {
        //支付数据
        $data['out_trade_no'] = $params['order_id'];
        $data['total_fee'] = (int)($params['goods_total_price'] * 100);
        $data['spbill_create_ip'] = $_SERVER["REMOTE_ADDR"];
        $data['attach'] = "微信支付";
        $data['body'] = mb_substr($params['body'], 0, 42);
        $data['appid'] = $this->config->appId;
        $data['mch_id'] = $this->config->mchId;
        $data['nonce_str'] = rand(000000000000, 999999999999);
        $data['trade_type'] = "NATIVE";
        $data['notify_url'] = $this->config->notifyUrl;

        $sign = $this->toUrlParams($data);
        $dataXML = "<xml>
           <appid>" . $data['appid'] . "</appid>
           <attach>" . $data['attach'] . "</attach>
           <body>" . $data['body'] . "</body>
           <mch_id>" . $data['mch_id'] . "</mch_id>
           <nonce_str>" . $data['nonce_str'] . "</nonce_str>
           <notify_url>" . $data['notify_url'] . "</notify_url>
           <out_trade_no>" . $data['out_trade_no'] . "</out_trade_no>
           <spbill_create_ip>" . $data['spbill_create_ip'] . "</spbill_create_ip>
           <total_fee>" . $data['total_fee'] . "</total_fee>
           <trade_type>" . $data['trade_type'] . "</trade_type>
           <sign>" . $sign . "</sign>
        </xml>";
        $url = 'https://api.mch.weixin.qq.com/pay/unifiedorder';
        $result = $this->httpsPost($url, $dataXML);
        $ret = $this->xmlToArray($result);

        return $ret;
    }

    /**
     * 查询订单
     * @param $order_id
     * @return bool
     */
    public function query($order_id)
    {
        $data['appid'] = $this->config->appId;
        $data['mch_id'] = $this->config->mchId;
        $data['nonce_str'] = rand(000000000000, 999999999999);
        $data['out_trade_no'] = $order_id;
        $sign = $this->toUrlParams($data);
        $dataXML = "<xml>
            <appid>" . $data['appid'] . "</appid>
            <mch_id>" . $data['mch_id'] . "</mch_id>
            <nonce_str>" . $data['nonce_str'] . "</nonce_str>
            <out_trade_no>" . $data['out_trade_no'] . "</out_trade_no>
            <sign>" . $sign . "</sign>
         </xml>";

        $url = 'https://api.mch.weixin.qq.com/pay/orderquery';
        $result = $this->httpsPost($url, $dataXML);
        $ret = $this->xmlToArray($result);

        if (isset($ret['return_code']) && $ret['return_code'] == 'SUCCESS' && $ret['return_msg'] == 'OK') {
            if (isset($ret['trade_state']) && $ret['trade_state'] == "SUCCESS") {
                return true;
            }
        }
        return false;
    }

    /**
     * POST提交数据
     * @param string $url
     * @param array $data
     * @param bool $ssl
     * @return bool|string
     */
    private function httpsPost($url, $data, $ssl = false)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        if ($ssl) {
            curl_setopt($ch, CURLOPT_SSLCERT, self::$wx_sslcert_path);
            curl_setopt($ch, CURLOPT_SSLKEY, self::$wx_sslkey_path);
        }
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            return 'Errno: ' . curl_error($ch);
        }
        curl_close($ch);
        return $result;
    }

    /**
     * XML转array
     * @param string $xml
     * @return mixed
     */
    private function xmlToArray($xml)
    {
        libxml_disable_entity_loader(true);
        $xmlString = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);
        return json_decode(json_encode($xmlString), true);
    }

    /**
     * 对参数排序，生成MD5加密签名
     * @param array $paramArray
     * @param bool $isencode
     * @return string
     */
    private function toUrlParams($paramArray, $isencode = false)
    {
        $paramStr = '';
        ksort($paramArray);
        $i = 0;
        foreach ($paramArray as $key => $value) {
            if ($key == 'Signature') {
                continue;
            }
            if ($i == 0) {
                $paramStr .= '';
            } else {
                $paramStr .= '&';
            }
            $paramStr .= $key . '=' . ($isencode ? urlencode($value) : $value);
            ++$i;
        }
        $stringSignTemp = $paramStr . "&key=" . $this->config->key;
        return strtoupper(md5($stringSignTemp));
    }

    /**
     * 读取配置项
     * @param $config
     */
    public function setOptions($config)
    {
        $this->config->appId = $config['appId'];
        $this->config->appSecret = $config['appSecret'];
        $this->config->mchId = $config['mchId'];
        $this->config->key = $config['key'];
        $this->config->signType = $config['signType'];
        $this->config->notifyUrl = $config['notifyUrl'];
        $this->config->sslCertPath = $config['sslCertPath'];
        $this->config->sslKeyPath = $config['sslKeyPath'];
        $this->config->appletsAppId = $config['appletsAppId'];
        $this->config->publicAppId = $config['publicAppId'];
    }
}