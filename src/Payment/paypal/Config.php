<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/12/9 15:49
 */

namespace Calculation\Payment\paypal;


class Config
{
    public $clientId;
    public $clientSecret;
    // public $sandboxClientId;
    // public $sandboxClientSecret;
    public $mode;
    public $httpProxy;
    public $httpConnectionTimeOut;
    public $notifyUrl;
    public $returnUrl;
}