<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/12/8 15:52
 */

namespace Calculation\Payment\alipay;


/**
 * Class 支付类
 */
class Pay
{
    private $config;

    public function __construct()
    {
        $this->config = new Config();
    }

    /**
     * 统一收单下单交易 下单方法
     * @param array $data
     * @return string|\提交表单HTML文本|\构建好的、签名后的最终跳转URL（GET）或String形式的form（POST）
     * @throws \Exception
     */
    public function pay($data)
    {
        require_once dirname(__DIR__) . '/alipay/alisdk/aop/AopCertClient.php';
        require_once dirname(__DIR__) . '/alipay/alisdk/aop/AopClient.php';
        require_once dirname(__DIR__) . '/alipay/alisdk/aop/request/AlipayTradeWapPayRequest.php';
        require_once dirname(__DIR__) . '/alipay/alisdk/aop/request/AlipayTradePagePayRequest.php';

        $c = new \AopCertClient();

        // $appCertPath = "应用证书路径（要确保证书文件可读），例如：/home/admin/cert/appCertPublicKey.crt";
        $appCertPath = $this->config->appCertPublicKey;
        // $alipayCertPath = "支付宝公钥证书路径（要确保证书文件可读），例如：/home/admin/cert/alipayCertPublicKey_RSA2.crt";
        $alipayCertPath = $this->config->alipayCertPublicKey;
        // $rootCertPath = "支付宝根证书路径（要确保证书文件可读），例如：/home/admin/cert/alipayRootCert.crt";
        $rootCertPath = $this->config->alipayRootCert;

        $c->gatewayUrl = $this->config->gatewayUrl;
        $c->appId = $this->config->appId;
        $c->rsaPrivateKey = $this->config->rsaPrivateKey;
        $c->format = "json";
        $c->charset = "UTF-8";
        $c->signType = $this->config->signType;

        //调用getPublicKey从支付宝公钥证书中提取公钥
        $c->alipayrsaPublicKey = $c->getPublicKey($alipayCertPath);
        //是否校验自动下载的支付宝公钥证书，如果开启校验要保证支付宝根证书在有效期内
        $c->isCheckAlipayPublicCert = true;
        //调用getCertSN获取证书序列号
        $c->appCertSN = $c->getCertSN($appCertPath);
        //调用getRootCertSN获取支付宝根证书序列号
        $c->alipayRootCertSN = $c->getRootCertSN($rootCertPath);
        # 如果是页面支付 不是手机 实例化具体API对应的request类,类名称和接口名称对应,当前调用接口名称：alipay.trade.app.pay

        $data['pay_type'] = isset($data['pay_type']) ? $data['pay_type'] : 'wap';
        if ($data['pay_type'] == 'page') {
            $product_code = "FAST_INSTANT_TRADE_PAY";
            $request = new \AlipayTradePagePayRequest();
        }

        if ($data['pay_type'] == 'wap') {
            $product_code = "QUICK_WAP_WAY";
            $request = new \AlipayTradeWapPayRequest();
        }

        //SDK已经封装掉了公共参数，这里只需要传入业务参数
        $request->setBizContent("{" .
            "    \"body\":\"" . $data['body'] . "\"," .
            "    \"subject\":\"" . $data['goods_name'] . "\"," .
            "    \"out_trade_no\":\"" . $data['order_id'] . "\"," .
            "    \"timeout_express\":\"90m\"," .
            "    \"total_amount\":" . $data['goods_total_price'] . "," .
            "    \"product_code\":\"" . $product_code . "\"" .
            // "    \"product_code\":\"QUICK_WAP_WAY\"" .
            "  }");

        //异步通知地址
        $request->setNotifyUrl($this->config->notifyUrl);

        //同步通知地址
        if (!isset($data['return_url'])) {
            $data['return_url'] = "";
        }
        $request->setReturnUrl($data['return_url']);

        //这里和普通的接口调用不同，使用的是sdkExecute
        $response = $c->pageExecute($request);

        //返回支付页面
        return $response;
    }

    /**
     * 查询订单
     * @param int $order_id 订单id
     * @return bool
     * @throws \Exception
     */
    public function query($order_id)
    {
        require_once dirname(__DIR__) . '/alipay/alisdk/aop/AopCertClient.php';
        require_once dirname(__DIR__) . '/alipay/alisdk/aop/AopClient.php';
        require_once dirname(__DIR__) . '/alipay/alisdk/aop/request/AlipayTradeWapPayRequest.php';
        require_once dirname(__DIR__) . '/alipay/alisdk/aop/request/AlipayTradeQueryRequest.php';

        $c = new \AopCertClient();

        // $appCertPath = "应用证书路径（要确保证书文件可读），例如：/home/admin/cert/appCertPublicKey.crt";
        $appCertPath = $this->config->appCertPublicKey;
        // $alipayCertPath = "支付宝公钥证书路径（要确保证书文件可读），例如：/home/admin/cert/alipayCertPublicKey_RSA2.crt";
        $alipayCertPath = $this->config->alipayCertPublicKey;
        // $rootCertPath = "支付宝根证书路径（要确保证书文件可读），例如：/home/admin/cert/alipayRootCert.crt";
        $rootCertPath = $this->config->alipayRootCert;
        $c->gatewayUrl = $this->config->gatewayUrl;
        $c->appId = $this->config->appId;
        $c->rsaPrivateKey = $this->config->rsaPrivateKey;
        $c->format = "json";
        $c->charset = "UTF-8";
        $c->signType = $this->config->signType;

        //调用getPublicKey从支付宝公钥证书中提取公钥
        $c->alipayrsaPublicKey = $c->getPublicKey($alipayCertPath);
        //是否校验自动下载的支付宝公钥证书，如果开启校验要保证支付宝根证书在有效期内
        $c->isCheckAlipayPublicCert = true;
        //调用getCertSN获取证书序列号
        $c->appCertSN = $c->getCertSN($appCertPath);
        //调用getRootCertSN获取支付宝根证书序列号
        $c->alipayRootCertSN = $c->getRootCertSN($rootCertPath);
        #如果是页面支付 不是手机 实例化具体API对应的request类,类名称和接口名称对应,当前调用接口名称：alipay.trade.app.pay

        $request = new \AlipayTradeQueryRequest();
        // "\"trade_no\":\"$order_id\"," .
        // "\"org_pid\":\".$c->appId.\"," .
        $request->setBizContent("{" .
            "\"out_trade_no\":\"$order_id\"," .
            "      \"query_options\":[" .
            "        \"TRADE_SETTE_INFO\"" .
            "      ]" .
            "  }");

        $result = $c->execute($request);
        if ($result->alipay_trade_query_response->code == 10000 && $result->alipay_trade_query_response->msg == 'Success') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 读取配置项
     * @param $config
     */
    public function setOptions($config)
    {
        $this->config->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
        $this->config->signType = $config['signType'];
        $this->config->appId = $config['appId'];

        // 为避免私钥随源码泄露，推荐从文件中读取私钥字符串而不是写入源码中
        $this->config->rsaPrivateKey = $config['rsaPrivateKey'];

        // 请填写您的支付宝公钥证书文件路径，例如：/foo/alipayCertPublicKey_RSA2.crt
        $this->config->alipayCertPublicKey = $config['alipayCertPublicKey'];
        // 请填写您的支付宝根证书文件路径，例如：/foo/alipayRootCert.crt
        $this->config->alipayRootCert = $config['alipayRootCert'];
        // 请填写您的应用公钥证书文件路径，例如：/foo/appCertPublicKey_2019051064521003.crt
        $this->config->appCertPublicKey = $config['appCertPublicKey'];

        //注：如果采用非证书模式，则无需赋值上面的三个证书路径，改为赋值如下的支付宝公钥字符串即可
        // $this->config->alipayPublicKey = '<-- 请填写您的支付宝公钥，例如：MIIBIjANBg... -->';

        //可设置异步通知接收服务地址（可选）
        $this->config->notifyUrl = $config['notifyUrl'];

        //可设置AES密钥，调用AES加解密相关接口时需要（可选）
        // $this->>config->encryptKey = "";
    }
}