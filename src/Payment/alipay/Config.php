<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/12/8 15:58
 */

namespace Calculation\Payment\alipay;


class Config
{
    public $appCertPublicKey;
    public $alipayCertPublicKey;
    public $alipayRootCert;
    public $gatewayUrl;
    public $appId;
    public $signType;
    public $rsaPrivateKey;
    public $notifyUrl;
}