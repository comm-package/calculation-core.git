# calculation-core

### 介绍
    calculation-core 彩析问卷调查报表计算包
    主要生成用户的问卷调查结果，计算答案相关逻辑
    以及阿里、微信、台湾、paypal支付

### 环境要求
    安装 Composer
    php7 或更高版本
    
### 软件架构
    - src 核心应用
	    - Payment 支付
	        - alipay 阿里支付
	        - ecpay  台湾支付
	        - paypal paypal支付
	        - wxpay  wxpay支付
	    - Questionnaire 报表计算
    - demo.php 生成问卷报表示例
    - alipaydemo.php 支付宝支付示例
    - ecpaydemo.php 台湾支付示例
    - paypalpaydemo.php paypal支付示例
    - wxpaydemo.php 微信支付示例
    
### 安装教程
    - composer require vars3cf/calculation-core (引用包)
    - composer.json  "vars3cf/calculation-core": "^1.0"